const express = require('express')
const path = require('path')
const http = require('http')
const Filter = require('bad-words')

const { generateMessage, generateLocMessage } = require('./utils/messages')
const { addUser, removeUser, getUser, getUsersInRoom } = require('./utils/users')

//set up socket io
const socketio = require('socket.io')

const app = express()
const server = http.createServer(app)

// set up instance of socket io
const io = socketio(server)

const port = process.env.PORT || 3000
const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))

// let count = 0

io.on('connection', (socket) => {
    console.log('New Websocket Connection')


    socket.on('join', (options, callback) => {

        const { user, error } = addUser({ id: socket.id, ...options })
        // possible because addUser return {user}

        if (error) {
            return callback(error)
        }
        socket.join(user.room)

        socket.emit('displayMessage', generateMessage('Admin', 'Welcome!'))
        socket.broadcast.to(user.room).emit('displayMessage', generateMessage('Admin', `${user.username} has joined`))
        io.to(user.room).emit('updateRoomUserList', {
            room: user.room,
            users: getUsersInRoom(user.room)
        })
        callback()
    })

    socket.on('sendMessage', (message, callback) => {
        const user = getUser(socket.id)


        const filter = new Filter()

        if (filter.isProfane(message)) {
            return callback('Profanity is not allowed')
        }
        io.to(user.room).emit('displayMessage', generateMessage(user.username, message))
        callback()
    })

    socket.on('sendLocation', (coords, callback) => {
        const user = getUser(socket.id)
        io.to(user.room).emit('displayLocation', generateLocMessage(user.username, `https://google.com/maps?q=${coords.lat},${coords.long}`))
        callback()
    })
    socket.on('disconnect', () => {
        const user = removeUser(socket.id)
        // using const instead of just function: returns data of removed user to be used
        // provided function returns the data

        if (user) {
            // do not need to use broadcast as the connection for the user has been terminated
            io.to(user.room).emit('displayMessage', generateMessage('Admin', `${user.username} has left`))
            io.to(user.room).emit('updateRoomUserList', {
                room: user.room,
                users: getUsersInRoom(user.room)
            })
        }



    })
})



server.listen(port, () => {
    console.log(`Server is up on port ${port}`)
})