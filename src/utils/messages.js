

const generateMessage = (username, text) => {
    return {
        username,
        text,
        createdAt: new Date()
    }
}

const generateLocMessage = (username, locUrl) => {
    return {
        username,
        locUrl,
        createdAt: new Date()
    }
}

// const generateUsersList = (roomData) => {
//     return {
//         room: roomData.room,
//         users: roomData.users
//     }
// }
// not needed as there is no other new parameter generated, unlike above with createdAt

module.exports = {
    generateMessage,
    generateLocMessage,

}