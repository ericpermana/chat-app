const users = []

// addUser, removeUser, getUser, getUsersInRoom

const processInput = (input) => {
    return input.trim().toLowerCase()
}

const addUser = ({ id, username, room }) => {
    // Clean the data
    username = processInput(username)
    room = processInput(room)

    // Validate the data
    if (!username || !room) {
        return {
            error: 'Username and room are required'
        }
    }

    // Check for existing user
    const existingUser = users.find((user) => {
        return user.room === room && user.username === username
    })

    // Validate username
    if (existingUser) {
        return {
            error: "Username is in use"
        }
    }

    // Store user
    const user = { id, username, room }
    users.push(user)
    return { user }

}

const removeUser = (id) => {
    const index = users.findIndex((user) => {
        return user.id === id
    })

    // findIndex return -1 when no match is found. The index number (0 or more) when there is match
    if (!index !== -1) {
        return users.splice(index, 1)[0]
    }
    // can also use filter. But filter will run even when match is found. 
    // findIndex will stop running when match is found
}

const getUser = (id) => {
    return users.find(user => user.id === id)
}

const getUsersInRoom = (room) => {
    room = processInput(room)
    return users.filter(user => user.room === room
    )
}

module.exports = {
    addUser,
    removeUser,
    getUser,
    getUsersInRoom
}

