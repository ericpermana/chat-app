const socket = io()
// Set up connection

const $messageForm = document.querySelector('#chat-form')
const $messageFormButton = document.querySelector('#send-message-btn')
const $messageFormInput = document.querySelector('#chat-input')
const $sendLocButton = document.querySelector('#send-geoloc-btn')
const $messages = document.querySelector('#chat-div')
const $sidebar = document.querySelector('#sidebar')

// Templates

const messageTemplate = document.querySelector('#message-template').innerHTML
const locMessageTemplate = document.querySelector('#loc-message-template').innerHTML
const sidebarTemplate = document.querySelector('#sidebar-template').innerHTML

// Options

const { username, room } = Qs.parse(location.search, { ignoreQueryPrefix: true })

const autscroll = () => {
    // New message element
    const $newMessage = $messages.lastElementChild

    // Height of the new message
    const newMessageStyles = getComputedStyle($newMessage)
    const newMessageMargin = parseInt(newMessageStyles.marginBottom)
    const newMessageHeight = $newMessage.offsetHeight + newMessageMargin

    // visible Height
    const visibleHeight = $messages.offsetHeight

    // Height of messages container
    const containerHeight = $messages.scrollHeight

    // How far user scrolled
    const scrollOffset = $messages.scrollTop + visibleHeight
    //There is not scrollBottom. So we add how far user has scrolled to the visible height

    // We delete newMessageHeight because calculation is done after new message is added

    if (containerHeight - newMessageHeight * 2 <= scrollOffset) {
        $messages.scrollTop = $messages.scrollHeight
    }
}

socket.on('displayMessage', (message) => {
    const html = Mustache.render(messageTemplate, {
        username: message.username,
        message: message.text,
        createdAt: moment(message.createdAt).format('h:mm a')
    })
    $messages.insertAdjacentHTML('beforeend', html)

    autscroll()
    // const para = document.createElement("p")
    // text = document.createTextNode(message)
    // para.appendChild(text)
    // $chatdiv.appendChild(para)
})

socket.on('displayLocation', (locMessage) => {
    const html = Mustache.render(locMessageTemplate, {
        username: locMessage.username,
        locUrl: locMessage.locUrl,
        createdAt: moment(locMessage.createdAt).format('h:mm a')
    })
    $messages.insertAdjacentHTML('beforeend', html)
    autscroll()
})

socket.on('updateRoomUserList', ({ room, users }) => {
    const html = Mustache.render(sidebarTemplate, {
        room,
        users
    })

    $sidebar.innerHTML = html

})

$messageForm.addEventListener('submit', (e) => {
    e.preventDefault()
    //disable form when sending
    $messageFormButton.setAttribute('disabled', 'disabled')

    console.log('Sending message')
    const chatMessage = e.target.elements.chatinput.value
    $messageFormInput.value = '' // emptying chat text box after click
    $messageFormInput.focus() // focus cursor to the text box

    socket.emit('sendMessage', chatMessage, (error) => {
        //enable
        $messageFormButton.removeAttribute('disabled')

        if (error) {
            return console.log(error)
        }
        console.log('Message Delivered')
    })



})

$sendLocButton.addEventListener('click', () => {


    if (!navigator.geolocation) {
        return alert('Geolocation is not supported by your browser')
    }
    $sendLocButton.setAttribute('disabled', 'disabled')

    navigator.geolocation.getCurrentPosition((position) => {

        socket.emit('sendLocation', {
            lat: position.coords.latitude,
            long: position.coords.longitude
        }, (geologAck) => {
            console.log('Location delivered.', geologAck)
            $sendLocButton.removeAttribute('disabled')
        })

    })

})

socket.emit('join', { username, room }, (error) => {
    if (error) {
        alert(error)
        location.href = '/'
    }
})

// the function must match up with what is in index.js to listen for emit from server
// socket.on('countUpdate', (count) => {
//     console.log('The count has been updated', count)
// })

// document.querySelector('#increment').addEventListener('click', () => {
//     console.log('Clicked')
//     socket.emit('increment') // This will emit from client to server
// })